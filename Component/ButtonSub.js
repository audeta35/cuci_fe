import React, {useState, useEffect, Fragment} from 'react';
import {useRouter} from 'next/router';
import {LocalMallTwoTone} from '@material-ui/icons';
import {Button, Spinner} from 'reactstrap';

const ButtonSub = (props) => {

    const [isLoading, setIsLoading] = useState(false)
    const router = useRouter();
    useEffect(() => {

        props._setInvoice();
    })

    const setInvoice = async () => {
        console.log(props?.cart)
        console.log(props?.userProfile)

        let url = `${window.location.origin}/api/transaction/set-invoice?idUser=${props?.userProfile?.id}&detailTransaction=${JSON.stringify(props?.cart)}`
        let req = await fetch(url);
        let res = await req.json();

        setIsLoading(true)
        Router.replace('/invoice');
    }

	return (

        <Fragment>
        {
            isLoading ? 

            <Button disabled color="primary">
                <Spinner size="sm" className="mr-2" />
                Lanjutkan
            </Button>

            :

            <Button color="primary" onClick={() => setInvoice()}>
                <LocalMallTwoTone className="mr-2" />
                Lanjutkan
            </Button>
        }
        </Fragment>
	)
}

export default ButtonSub;