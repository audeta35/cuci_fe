import React, { useEffect, useState } from 'react';
import { Divider } from '@material-ui/core';
import { EventTwoTone, MonetizationOnTwoTone, SupervisedUserCircleTwoTone } from '@material-ui/icons';
import seat from '../assets/services/seat.jpg';
import { Card, CardHeader, CardBody, CardFooter, Media, Row, Col, Button, Spinner } from 'reactstrap';
import { useRouter } from 'next/router';
import constants from '../service/constants';

const ListInvoice = (props) => {

    let { items } = props;
    const [detail, setDetail] = useState({});
    
    const router = useRouter();
    const [isLoading, setIsLoading] = useState(false);

    useEffect(() => {
        if(items) {
            console.log("detail", JSON.parse(items.detail_transaction))
            console.log("items", props?.items)
            setDetail(JSON.parse(items.detail_transaction))
        }
    }, [])

    const pushLink = (id) => {

        setIsLoading(true)
        router.push(`/pesanan/${id}`)
    }

    const numberFormat = (amount) => {
        let num = new Intl.NumberFormat('id-ID').format(amount);
        return num;
    }

    const convertDate = (dates) => {
        const dt = new Date(dates);
        return `${dt.getDate()}/${constants.MONTHS[dt.getMonth()]}/${dt.getFullYear()}`
    }

    return (

        <Card className="my-4 shadow-sm">
            <CardHeader className="text-secondary bg-white">
                <span className="badge badge-primary">{detail?.payMethod}</span> | 
                <span className="badge badge-dark">{items?.status}</span>
            </CardHeader>
            <CardBody>
                <Media>
                    <Media body className="mx-2">
                        <small>
                            <b>
                                {detail?.serviceName} | Pukul {detail?.timeOrder}
                            </b>
                        </small>
                        <Divider className="my-1" />
                        <Row>
                            <Col xs={6}>
                                <small>
                                    <EventTwoTone className="mr-1" />
                                    <b>{convertDate(detail?.dateOrder)}</b>
                                </small>
                            </Col>

                            <Col xs={6}>
                                <small>
                                    <MonetizationOnTwoTone className="mr-1" />
                                    <b>Rp{numberFormat(detail?.totalPrice)}</b>
                                </small>
                            </Col>
                        </Row>
                    </Media>
                </Media>
            </CardBody>
            <CardFooter className="bg-white">
            {
                isLoading ? 
                <Button className="stretched-link" color="primary" onClick={() => pushLink(items?.id)} block disabled>
                    <Spinner size="sm"/> Detail pesanan
                </Button>
                :
                <Button className="stretched-link" color="primary" onClick={() => pushLink(items?.id)} block outline>
                    Detail pesanan
                </Button>
            }
            </CardFooter>
        </Card>
    )
}

export default ListInvoice;
