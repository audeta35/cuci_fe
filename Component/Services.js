import React, { Fragment } from 'react';

import Link from 'next/link';
import {
    Card,CardImg, CardImgOverlay, CardText, CardBody,
    CardTitle, CardSubtitle,
    Button, Badge,
} from 'reactstrap';
import { List, ListItem, ListItemText, ListItemAvatar } from '@material-ui/core';
import { BusinessCenterTwoTone } from '@material-ui/icons';

import {useRouter} from 'next/router';

const Services = (props) => {

    const router = useRouter();
    const pushLink = (path, id) => {

        router.push(`/jasa/${path}/${id}`);
    }

    return (

        <Fragment>
            <ListItem button className="shadow-sm bg-white rounded text-primary" onClick={() => pushLink(props?.data?.sub_title?.toLowerCase() || "", props?.data?.id)}>
                <BusinessCenterTwoTone />
                <ListItemText 
                className="ml-3"
                primary={props?.data?.title} 
                secondary={
                    <small> {props?.data?.sub_title} </small>
                } 
                />
            </ListItem>  
        </Fragment>
    );
};

export default Services;
