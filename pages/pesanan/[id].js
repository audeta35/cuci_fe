import React, { Component, Fragment } from 'react';

import Router, { withRouter } from 'next/router';
import { Container, ListItem, ListItemText, Divider } from '@material-ui/core';
import { ListGroup, ListGroupItem, Row, Col, Table, Card, CardBody, CardFooter, Badge } from 'reactstrap';
import { ReceiptTwoTone, AccountBoxTwoTone, EmailTwoTone, AccountBalanceWalletTwoTone, TodayTwoTone, EventTwoTone, PhoneAndroidTwoTone } from '@material-ui/icons';
import { Skeleton } from '@material-ui/lab';
import Navbars from '../../Component/Navbars';
class invoice extends Component {

    constructor(props) {

        super(props);
        console.log("props router query", props?.router?.query)
        this.state = {
            
            id: props?.router?.query?.id,
            invoice: {},
            detail_invoice: {},
            userDetail: {},

            number: 1,
            serviceName: "Cuci Sofa",
            product: [{
                id: 1,
                price: 40000,
                type: "item",
                label: "Sofa"
            },
            {
                id: 2,
                price: 160000,
                type: "item",
                label: "Sofa Bed"
            },
            {
                id: 3,
                price: 10000,
                type: "item",
                label: "Bantal Sofa"
            },
            {
                id: 4,
                price: 80000,
                type: "item",
                label: "Karpet Sofa"
            }],
        }
    }

    componentDidMount() {
        if(this.state.id) {
            this._getTransactionById(this.state.id);
        }
    }

    _getTransactionById = async (id) => {
        let url = `${window.location.origin}/api/transaction/get-invoice?idTransaction=${id}`
        let req = await fetch(url)
        let res = await req.json()

        if(res?.status?.code === 200 && res?.result?.length !== 0) {
            this?._getUserProfile(res?.result[0]?.id_user)
            this.setState({
                ...this.state,
                invoice: res?.result[0],
                detail_invoice: JSON.parse(res?.result[0]?.detail_transaction)
            })

            console.log("transaction: ", res?.result[0])
            console.log("detail transaction: ", JSON.parse(res?.result[0]?.detail_transaction))
        }
    }

    _getUserProfile = async (id) => {
        let url = `${window.location.origin}/api/users/getById?id=${id}`
        let req = await fetch(url);
        let res = await req.json();

        console.log(res)
        console.log(id)

        if(res?.status?.code === 200 && res?.result?.length !== 0) {
            this.setState({
                ...this.state,
                userDetail: res?.result[0]
            })
        }
    }

    render() {

        return (

            <Fragment>
                <Navbars navbrand="Detail Pesanan" stack={true} />
                <Container maxWidth="xs">
                    <Card className="my-4">
                        <CardBody>
                            <h3>
                                <b className="text-primary">
                                    {this?.state?.detail_invoice?.serviceName}
                                </b>
                            </h3>

                            <Divider />

                            <Row className="mb-2">
                                <Col xs={6}>
                                    <div className="my-2">
                                        <small>
                                            <AccountBoxTwoTone className="mr-1" />
                                            {this?.state?.userDetail?.username}
                                        </small>
                                    </div>

                                    <div className="my-2">
                                        <small>
                                            <PhoneAndroidTwoTone className="mr-1" />
                                            {this?.state?.userDetail?.phone}
                                        </small>
                                    </div>

                                    <div className="my-2">
                                        <small>
                                            <AccountBalanceWalletTwoTone className="mr-1" />
                                            {this?.state?.detail_invoice?.payMethod}
                                        </small>
                                    </div>
                                </Col>

                                <Col xs={6}>
                                    <div className="my-2">
                                        <small>
                                            {this?.state?.detail_invoice?.getAddress?.namaJalan}, <br />
                                            {this?.state?.detail_invoice?.getAddress?.cluster} <br />
                                            {this?.state?.detail_invoice?.getAddress?.nomorAlamat} - {this?.state?.detail_invoice?.getAddress?.RT}/{this?.state?.detail_invoice?.getAddress?.RW} <br />
                                            {this?.state?.detail_invoice?.getAddress?.kecamatan}, {this?.state?.detail_invoice?.getAddress?.kabupaten}, {this?.state?.detail_invoice?.getAddress?.provinsi}
                                        </small>
                                    </div>
                                </Col>
                            </Row>

                            <Divider />

                            <Table size="sm" className="bg-white" borderless hover striped>
                                <thead className="bg-primary text-white">
                                    <tr>
                                        <th>#</th>
                                        <th>Jasa {this.state.serviceName}</th>
                                        <th>Satuan</th>
                                        <th>harga (Rp)</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {
                                        this.state.product.map(res => (

                                            <tr key={res.id}>
                                                <th scope="row">{this.state.number++}</th>
                                                <td> {res.label} <b>({res.id})</b> </td>
                                                <td> {res.type} </td>
                                                <td> {res.price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")} </td>
                                            </tr>

                                        ))
                                    }
                                </tbody>
                                <thead className="bg-primary text-white">
                                    <tr>
                                        <th colspan={3} className="text-center">Jumlah Pembayaran</th>
                                        <th>Rp 290.000</th>
                                    </tr>
                                </thead>
                            </Table>

                            <Divider />

                            <Row>
                                <Col xs={5}>
                                    <ListItem>
                                        <TodayTwoTone className="mr-2" />
                                        <ListItemText
                                            primary="Dipesan"
                                            secondary={
                                                <Badge color="primary">03/04/2020</Badge>
                                            }
                                        />
                                    </ListItem>
                                </Col>

                                <Col xs={7}>
                                    <ListItem>
                                        <EventTwoTone className="mr-2" />
                                        <ListItemText
                                            primary="Dikerjakan"
                                            secondary={
                                                <Badge color="primary">03/04/2020 13.00</Badge>
                                            }
                                        />
                                    </ListItem>
                                </Col>
                            </Row>

                            <Divider />
                        </CardBody>
                    </Card>

                    <ListItem className="bg-primary text-white mb-3">
                        <ListItemText primary="Status Invoce :" />
                        <Badge color="light">
                            Selesai Dikerjakan
                        </Badge>
                    </ListItem>
                </Container>
            </Fragment>
        )
    }
}

export default withRouter(invoice);