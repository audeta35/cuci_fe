import React, { Component, Fragment, useEffect, useState } from 'react';
import { withRouter } from 'next/router';
import Navbars from '../Component/Navbars';
import { Container, Divider } from '@material-ui/core';
import { EventTwoTone, MonetizationOnTwoTone, SupervisedUserCircleTwoTone } from '@material-ui/icons';
import seat from '../assets/services/seat.jpg';
import { Card, CardHeader, CardBody, CardFooter, Media, Row, Col } from 'reactstrap';
import ListInvoice from '../Component/ListInvoice';
import AppBars from '../Component/AppBars';
import fetch from 'node-fetch';

const PesananPage = () => {

    const token = window.sessionStorage.getItem('token') || "";
    const [invoice, setInvoice] = useState([]);

    useEffect(() => {
        if (token) {
            getUserByToken(token);
        }
    }, [])

    const getUserByToken = async (token) => {
        let url = `${window.location.origin}/api/users/getByToken?token=${token}`
        let req = await fetch(url)
        const res = await req.json();

        getInvoice(res?.result[0]?.id)
    }

    const getInvoice = async (id) => {
        let url = `${window.location.origin}/api/transaction/get-by-id?idUser=${id}`
        let req = await fetch(url)
        let res = await req.json()
        if (res?.result) {
            setInvoice(res.result);
        }
    }

    return (
        <Fragment>
            <Navbars navbrand="Pesanan" />
            <Container maxWidth="xs">
                {
                    invoice?.length !== 0 ?
                        invoice.map((item, index) => (
                            <ListInvoice keys={index} items={item} />
                        ))
                        : (
                            <div className="container text-center my-5">
                                <i className="text-center text-muted">Belum ada pesanan</i>
                            </div>
                        )
                }
            </Container>
            <br />
            <br />
            <br />
            <AppBars />
        </Fragment>
    )
}

export default PesananPage;
