import fetch from 'node-fetch';
export default async (req, res) => {

    let url = "http://localhost:3100"

    const result = await fetch(`${url}/service/get-all`, {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
    })

    const data = await result.json();
    res.json(data);
}