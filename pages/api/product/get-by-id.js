import fetch from 'node-fetch';
export default async (req, res) => {

    let {id} = req.query;

    const payload = {
        id: id
    }

    let url = "http://localhost:3100"

    const result = await fetch(`${url}/product/get-product`, {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(payload)
    })

    const data = await result.json();
    res.json(data);
}