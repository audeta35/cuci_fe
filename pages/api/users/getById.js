import fetch from 'node-fetch';
export default async (req, res) => {

    let url = "http://localhost:3100"
    let { id } = req.query

    const payload = {
        id: id
    }

    const result = await fetch(`${url}/users/get-profile`, {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(payload)
    })

    const data = await result.json();
    res.json(data);
}