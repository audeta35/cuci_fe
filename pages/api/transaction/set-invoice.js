import fetch from 'node-fetch';
export default async (req, res) => {

    let url = "http://localhost:3100"
    let { idUser, detailTransaction } = req.query

    const payload = {
        idUser: idUser,
        detailTransaction: detailTransaction
    }

    const result = await fetch(`${url}/transaction/set-invoice`, {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(payload)
    })

    const data = await result.json();
    res.json(data);
}