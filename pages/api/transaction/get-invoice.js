import fetch from 'node-fetch';
export default async (req, res) => {

    let url = "http://localhost:3100"
    let { idTransaction } = req.query

    const payload = {
        id_transaction: idTransaction,
    }

    const result = await fetch(`${url}/transaction/get-invoice`, {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(payload)
    })

    const data = await result.json();
    res.json(data);
}