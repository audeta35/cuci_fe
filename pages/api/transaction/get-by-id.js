import fetch from 'node-fetch';
export default async (req, res) => {

    let url = "http://localhost:3100"
    let { idUser } = req.query

    const payload = {
        idUser: idUser,
    }

    const result = await fetch(`${url}/transaction/get-by-id`, {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(payload)
    })

    const data = await result.json();
    res.json(data);
}